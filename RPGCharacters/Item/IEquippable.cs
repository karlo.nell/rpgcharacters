﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public interface IEquippable
    {
        //Weapon Management
        Weapon EquippedWeapon { get; set; }
        public string EquipWeapon(Weapon Weapon);
        public bool IsEquippableWeapon(Weapon Weapon, List<WeaponType> weaponTypes);
        public bool IsEquippableWeaponType(Weapon Weapon, List<WeaponType> weaponTypes);
        public List<WeaponType> ValidWeaponTypes();
        public double CalculateDPS();


        //Armor management
        public string EquipArmor(Armor Armor);
        public bool IsEquippableArmor(Armor armor, List<ArmorType> armorTypes);
        public bool IsEquippableArmorType(Armor armor, List<ArmorType> armorTypes);
        public List<ArmorType> ValidArmorTypes();
        public void InitializeEmptyArmorSlots();
        public void AddArmorAttributesToCharacterStats(Armor armor);


        //Other
        public int GetCharacterIncreasedDamageAttribute();
    }
}
