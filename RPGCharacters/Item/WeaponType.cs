﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public enum WeaponType
    {
        WEAPON_STAFF,
        WEAPON_WAND,
        WEAPON_BOW,
        WEAPON_DAGGER,
        WEAPON_SWORD,
        WEAPON_AXE, 
        WEAPON_HAMMER
    }
}
