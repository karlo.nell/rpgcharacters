﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Armor : Item
    {
        public PrimaryAttributes PrimaryAttribute { get; set; }
        public ArmorType ArmorType { get; set; }
    }
}
