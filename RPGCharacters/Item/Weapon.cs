﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Weapon : Item
    {
        public WeaponAttribute WeaponAttributes { get; set; }
        public WeaponType WeaponType { get; set; }

        public Weapon()
        {
            
        }
        
    }
}
