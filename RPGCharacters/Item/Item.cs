﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public abstract class Item
    {
        public int ItemLevel { get; set; }
        public string ItemName { get; set; }
        public Slot ItemSlot { get; set; }
    }
}
