﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public static class Db
    {    
        //The "database"
        #region Mage - Attributes and EquippableTypes
        public static PrimaryAttributes MageStartPrimaryAttributes { get; } = new PrimaryAttributes()
        {
            Dexterity = 1,
            Intelligence = 8,
            Strength = 1,
            Vitality = 5
        };
        public static PrimaryAttributes MagePrimaryAttributeGainOnLevel { get; } = new PrimaryAttributes()
        {
            Dexterity = 1,
            Intelligence = 5,
            Strength = 1,
            Vitality = 3
        };
        public static List<WeaponType> MageEquippableWeapons  = new List<WeaponType> { WeaponType.WEAPON_STAFF, WeaponType.WEAPON_WAND };
        public static List<ArmorType> MageEquippableArmor = new List<ArmorType> { ArmorType.ARMOR_CLOTH };
        #endregion
        #region Ranger - Attributes and EquippableTypes
        public static PrimaryAttributes RangerStartPrimaryAttributes { get; } = new PrimaryAttributes()
        {
            Dexterity = 7,
            Intelligence = 1,
            Strength = 1,
            Vitality = 8
        };
        public static PrimaryAttributes RangerPrimaryAttributeGainOnLevel { get; } = new PrimaryAttributes()
        {
            Dexterity = 5,
            Intelligence = 1,
            Strength = 1,
            Vitality = 2
        };
        public static List<WeaponType> RangerEquippableWeapons { get; } = new List<WeaponType> { };
        public static List<ArmorType> RangerEquippableArmor { get; } = new List<ArmorType> { ArmorType.ARMOR_LEATHER, ArmorType.ARMOR_MAIL };
        #endregion
        #region Rogue - Attributes and EquippableTypes
        public static PrimaryAttributes RogueStartPrimaryAttributes { get; } = new PrimaryAttributes()
        {
            Dexterity = 6,
            Intelligence = 1,
            Strength = 2,
            Vitality = 8
        };
        public static PrimaryAttributes RoguePrimaryAttributeGainOnLevel { get; } = new PrimaryAttributes()
        {
            Dexterity = 4,
            Intelligence = 1,
            Strength = 1,
            Vitality = 3
        };
        public static List<WeaponType> RogueEquippableWeapons { get; } = new List<WeaponType> { WeaponType.WEAPON_DAGGER, WeaponType.WEAPON_SWORD };
        public static List<ArmorType> RogueEquippableArmor { get; } = new List<ArmorType> { ArmorType.ARMOR_LEATHER, ArmorType.ARMOR_MAIL };

        #endregion
        #region Warrior - Attributes and EquippableTypes
        public static PrimaryAttributes WarriorStartPrimaryAttributes { get; } = new PrimaryAttributes()
        {
            Dexterity = 2,
            Intelligence = 1,
            Strength = 5,
            Vitality = 10
        };
        public static PrimaryAttributes WarriorPrimaryAttributeGainOnLevel { get; } = new PrimaryAttributes()
        {
            Dexterity = 2,
            Intelligence = 1,
            Strength = 3,
            Vitality = 5
        };
        public static List<WeaponType> WarriorEquippableWeapons { get;} = new List<WeaponType> { WeaponType.WEAPON_AXE, WeaponType.WEAPON_HAMMER, WeaponType.WEAPON_SWORD };
        public static List<ArmorType> WarriorEquippableArmor { get;} = new List<ArmorType> { ArmorType.ARMOR_PLATE, ArmorType.ARMOR_MAIL };
        #endregion
    }
}
