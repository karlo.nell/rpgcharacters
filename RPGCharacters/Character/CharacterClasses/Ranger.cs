﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.CharacterClasses
{
    public class Ranger : Character
    {
        /// <summary>
        /// Each time a ranger class is created it runs InitializeClassPrimaryAttributes() and calculcates the secondary attributes
        /// </summary>
        public Ranger()
        {
            InitializeClassPrimaryAttributes();

            TotalPrimaryAttribute.CalculateSecondaryAttributes(SecondaryAttributes);
        }
        /// <summary>
        /// Initializes the current class's primary attributes with values stored in Db class.
        /// </summary>
        public override void InitializeClassPrimaryAttributes()
        {
            TotalPrimaryAttribute.Dexterity += Db.RangerStartPrimaryAttributes.Dexterity;
            TotalPrimaryAttribute.Intelligence += Db.RangerStartPrimaryAttributes.Intelligence;
            TotalPrimaryAttribute.Strength += Db.RangerStartPrimaryAttributes.Strength;
            TotalPrimaryAttribute.Vitality += Db.RangerStartPrimaryAttributes.Vitality;
        }

    }
}
