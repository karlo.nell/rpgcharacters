﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class PrimaryAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Vitality { get; set; }
        
        /// <summary>
        /// Calculates the secondary attributes based on the primary attributes.
        /// </summary>
        /// <param name="secondaryAttribute">Secondary Attribute object</param>
        public void CalculateSecondaryAttributes(SecondaryAttributes secondaryAttribute)
        {
            secondaryAttribute.Health = Vitality * 10;
            secondaryAttribute.ArmorRating = Strength + Dexterity;
            secondaryAttribute.ElementalResistance = Intelligence;
        }

        /// <summary>
        /// Adds the level up attribute gain to the current class' primary attribute based on how many levels gained. 
        /// </summary>
        /// <param name="type">Type of Character.</param>
        /// <param name="levelsToLevelUp">Amount of levels to level up.</param>
        internal void LevelUp(Type type, int levelsToLevelUp)
        {
            for (int i = 0; i < levelsToLevelUp; i++)
            {
                Dexterity += FindClassPrimaryAttributeGain(type).Dexterity;
                Intelligence += FindClassPrimaryAttributeGain(type).Intelligence;
                Strength += FindClassPrimaryAttributeGain(type).Strength;
                Vitality += FindClassPrimaryAttributeGain(type).Vitality;
            }
        }

        /// <summary>
        /// Finds the primary attribute for the specified Character type.
        /// </summary>
        /// <param name="type">Type of Character.</param>
        /// <returns>PrimaryAttribute of the Character type. Returns null if not found.</returns>
        public PrimaryAttributes FindClassPrimaryAttributeGain(Type type)
        {
            switch (type.Name)
            {
                case "Mage":
                    return Db.MagePrimaryAttributeGainOnLevel;
                case "Ranger":
                    return Db.RangerPrimaryAttributeGainOnLevel;
                case "Rogue":
                    return Db.RoguePrimaryAttributeGainOnLevel;
                case "Warrior":
                    return Db.WarriorPrimaryAttributeGainOnLevel;
                default:
                    return null;
            }
        }

        ///<summary> 
        /// Checks if two PrimaryAttributes objects are equal. 
        /// </summary> 
        /// /// <param name="obj">Object to compare to</param> 
        /// /// <returns>True if objects are equal, otherwise false</returns> 
        public override bool Equals(object obj) 
        { 
            return obj is PrimaryAttributes attributes && 
                Strength == attributes.Strength && 
                Dexterity == attributes.Dexterity && 
                Intelligence == attributes.Intelligence && 
                Vitality == attributes.Vitality; 
        }
    }
}
